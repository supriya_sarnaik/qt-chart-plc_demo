QT += charts
requires(qtConfig(combobox))

HEADERS += \
    themewidget.h

SOURCES += \
    main.cpp \
    themewidget.cpp

target.path = /tmp/$${TARGET}/bin
INSTALLS += target

FORMS += \
    themewidget.ui
