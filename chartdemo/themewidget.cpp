#include "themewidget.h"
#include "ui_themewidget.h"

#include <QtCharts/QChartView>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>
#include <QtCharts/QAbstractBarSeries>
#include <QtCharts/QPercentBarSeries>
#include <QtCharts/QStackedBarSeries>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLineSeries>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QAreaSeries>
#include <QtCharts/QLegend>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtCore/QRandomGenerator>
#include <QtCharts/QBarCategoryAxis>
#include <QtWidgets/QApplication>
#include <QtCharts/QValueAxis>
#include <QtWidgets/QGesture>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsView>
#include <QtGui/QMouseEvent>
#include <QDebug>
int count = 5;
QChartView *chartView;
ThemeWidget::ThemeWidget(QWidget *parent) :
    QWidget(parent),
    m_listCount(3),
    m_valueMax(10),
    m_valueCount(7),
    m_dataTable(generateRandomData(m_listCount, m_valueMax, m_valueCount)),
    m_ui(new Ui_ThemeWidgetForm)

{
    m_ui->setupUi(this);
    populateThemeBox();
    populateAnimationBox();
    populateLegendBox();
    //create charts

    //    QChartView *chartView;

    chartView = new QChartView(createLineChart());
    chartView->setRubberBand(QChartView::RectangleRubberBand);
    //    chartView->chart()->scroll(0,1);
    m_ui->gridLayout->addWidget(chartView, 1, 0);
    m_charts << chartView;

    chartView = new QChartView(createBarChart(m_valueCount));
    chartView->setRubberBand(QChartView::RectangleRubberBand);
    //    chartView->chart()->scroll(0,1);
    m_ui->gridLayout->addWidget(chartView, 2, 0);
    m_charts << chartView;


    // Set defaults
    m_ui->antialiasCheckBox->setChecked(true);

    // Set the colors from the light theme as default ones
    QPalette pal = qApp->palette();
    pal.setColor(QPalette::Window, QRgb(0x40434a));
    pal.setColor(QPalette::WindowText, QRgb(0xd6d6d6));
    qApp->setPalette(pal);

    updateUI();
}

ThemeWidget::~ThemeWidget()
{
    delete m_ui;
}

DataTable ThemeWidget::generateRandomData(int listCount, int valueMax, int valueCount) const
{
    DataTable dataTable;

    // generate random data
    for (int i(0); i < listCount; i++) {
        DataList dataList;
        qreal yValue(0);
        for (int j(0); j < valueCount; j++) {
            yValue = yValue + QRandomGenerator::global()->bounded(valueMax / (qreal) valueCount);
            QPointF value((j + QRandomGenerator::global()->generateDouble()) * ((qreal) m_valueMax / (qreal) valueCount),
                          yValue);
            QString label = "Slice " + QString::number(i) + ":" + QString::number(j);
            dataList << Data(value, label);
        }
        dataTable << dataList;
    }

    return dataTable;
}

void ThemeWidget::populateThemeBox()
{
    // add items to theme combobox
    m_ui->themeComboBox->addItem("Blue Cerulean", QChart::ChartThemeBlueCerulean);
    m_ui->themeComboBox->addItem("Light", QChart::ChartThemeLight);
    m_ui->themeComboBox->addItem("Dark", QChart::ChartThemeDark);
    m_ui->themeComboBox->addItem("Brown Sand", QChart::ChartThemeBrownSand);
    m_ui->themeComboBox->addItem("Blue NCS", QChart::ChartThemeBlueNcs);
    m_ui->themeComboBox->addItem("High Contrast", QChart::ChartThemeHighContrast);
    m_ui->themeComboBox->addItem("Blue Icy", QChart::ChartThemeBlueIcy);
    m_ui->themeComboBox->addItem("Qt", QChart::ChartThemeQt);
}

void ThemeWidget::populateAnimationBox()
{
    // add items to animation combobox
    m_ui->animatedComboBox->addItem("GridAxis Animations", QChart::GridAxisAnimations);
    m_ui->animatedComboBox->addItem("No Animations", QChart::NoAnimation);
    //    m_ui->animatedComboBox->addItem("Series Animations", QChart::SeriesAnimations);
    m_ui->animatedComboBox->addItem("All Animations", QChart::AllAnimations);
}

void ThemeWidget::populateLegendBox()
{
    // add items to legend combobox
    m_ui->legendComboBox->addItem("No Legend ", 0);
    m_ui->legendComboBox->addItem("Legend Top", Qt::AlignTop);
    m_ui->legendComboBox->addItem("Legend Bottom", Qt::AlignBottom);
    m_ui->legendComboBox->addItem("Legend Left", Qt::AlignLeft);
    m_ui->legendComboBox->addItem("Legend Right", Qt::AlignRight);
}

QChart *ThemeWidget::createBarChart(int valueCount) const
{
    Q_UNUSED(valueCount);
    QChart *chart = new QChart();
    chart->setTitle("Bar chart");
    chart->scroll(0,1);
    //    QStackedBarSeries *series = new QStackedBarSeries(chart);
    //    for (int i(0); i < m_dataTable.count(); i++) {
    //        QBarSet *set = new QBarSet("Bar set " + QString::number(i));
    //        for (const Data &data : m_dataTable[i])
    //            *set << data.first.y();
    //        series->append(set);
    //    }
    QBarSet *set = new QBarSet("Bar set ");

    switch (count) {
    case 7:
        qDebug()<< "case 7: " <<count;
        *set << 1 << 1 << 3 << 5 << 3 << 0;
        break;
    case 8:
        qDebug()<< "case 8: " <<count;
        *set << 5 << 2 << 6 << 8 << 4 << 0;
        break;
    case 9:
        qDebug()<< "case 9: " <<count;
        *set << 0 << 7 << 9 << 11 << 13 << 0;

        break;
    default:
        qDebug()<< "default: " <<count;
        *set << 2 << 5 << 10 << 15 << 8 << 4;
        count = 6;
        break;

    }


//    *set << 0 << 2 << 6 << 8 << 4 << 0;
    QBarSeries *series1 = new QBarSeries();
    series1->append(set);
    chart->addSeries(series1);

    chart->createDefaultAxes();
    chart->axes(Qt::Vertical).first()->setRange(0, m_valueMax * 2);
    // Add space to label to add space between labels and axis
    QValueAxis *axisY = qobject_cast<QValueAxis*>(chart->axes(Qt::Vertical).first());
    Q_ASSERT(axisY);
    axisY->setLabelFormat("%g  ");
    chart->zoomOut();

    return chart;
}

QChart *ThemeWidget::createLineChart() const
{
    QChart *chart = new QChart();
    chart->setTitle("Line chart");
    QString name("Series ");
    /*   int nameIndex = 0;
    for (const DataList &list : m_dataTable) {
        QLineSeries *series = new QLineSeries(chart);
        for (const Data &data : list)
            series->append(data.first);
        series->setName(name + QString::number(nameIndex));
        nameIndex++;
        chart->addSeries(series);
    }

        chart->axes(Qt::Horizontal).first()->setRange(0, m_valueMax);
        chart->axes(Qt::Vertical).first()->setRange(0, m_valueCount);*/
    qDebug()<< "count: " <<count;
    QLineSeries *lineseries = new QLineSeries();
    lineseries->setName("trend");
    //       lineseries->append(0, 0);
    switch (count) {
    case 7:
        qDebug()<< "case 7: " <<count;
        lineseries->append(1, 1);
        lineseries->append(2, 1);
        lineseries->append(3, 3);
        lineseries->append(4, 5);
        lineseries->append(5, 3);
        lineseries->append(6, 0);
        break;
    case 8:
        qDebug()<< "case 8: " <<count;
        lineseries->append(1, 5);
        lineseries->append(2, 2);
        lineseries->append(3, 6);
        lineseries->append(4, 8);
        lineseries->append(5, 4);
        lineseries->append(6, 0);
        break;
    case 9:
        qDebug()<< "case 9: " <<count;
        lineseries->append(1, 0);
        lineseries->append(2, 7);
        lineseries->append(3, 9);
        lineseries->append(4, 11);
        lineseries->append(5, 13);
        lineseries->append(6, 0);
        break;
    default:
        qDebug()<< "default: " <<count;
        lineseries->append(1, 2);
        lineseries->append(2, 5);
        lineseries->append(3, 10);
        lineseries->append(4, 15);
        lineseries->append(5, 8);
        lineseries->append(6, 4);
        count = 6;
        break;

    }
    //    lineseries->append(1, 0);
    //    lineseries->append(2, 2);
    //    lineseries->append(3, 6);
    //    lineseries->append(4, 8);
    //    lineseries->append(5, 4);
    //    lineseries->append(6, 0);
    chart->addSeries(lineseries);

    chart->createDefaultAxes();
    chart->axes(Qt::Horizontal).first()->setRange(0, 6);
    chart->axes(Qt::Vertical).first()->setRange(0, 20);
    QValueAxis *axisY = qobject_cast<QValueAxis*>(chart->axes(Qt::Vertical).first());
    Q_ASSERT(axisY);
    axisY->setLabelFormat("%g  ");
    axisY->setTickCount(5);

    QValueAxis *axisX = qobject_cast<QValueAxis*>(chart->axes(Qt::Horizontal).first());
    Q_ASSERT(axisX);
    axisX->setLabelFormat("%g  ");
    axisX->setTickCount(7);
    axisX->setTickInterval(2);
    chart->setAcceptHoverEvents(true);
    chart->zoomOut();
    return chart;
}

void ThemeWidget::updateUI()
{
    count ++;
    QChart::ChartTheme theme = static_cast<QChart::ChartTheme>(
                m_ui->themeComboBox->itemData(m_ui->themeComboBox->currentIndex()).toInt());
    const auto charts = m_charts;
    if (!m_charts.isEmpty() && m_charts.at(0)->chart()->theme() != theme) {
        for (QChartView *chartView : charts) {
            chartView->chart()->setTheme(theme);
        }

        // Set palette colors based on selected theme
        QPalette pal = window()->palette();
        if (theme == QChart::ChartThemeLight) {
            pal.setColor(QPalette::Window, QRgb(0xf0f0f0));
            pal.setColor(QPalette::WindowText, QRgb(0x404044));
        } else if (theme == QChart::ChartThemeDark) {
            pal.setColor(QPalette::Window, QRgb(0x121218));
            pal.setColor(QPalette::WindowText, QRgb(0xd6d6d6));
        } else if (theme == QChart::ChartThemeBlueCerulean) {
            pal.setColor(QPalette::Window, QRgb(0x40434a));
            pal.setColor(QPalette::WindowText, QRgb(0xd6d6d6));
        } else if (theme == QChart::ChartThemeBrownSand) {
            pal.setColor(QPalette::Window, QRgb(0x9e8965));
            pal.setColor(QPalette::WindowText, QRgb(0x404044));
        } else if (theme == QChart::ChartThemeBlueNcs) {
            pal.setColor(QPalette::Window, QRgb(0x018bba));
            pal.setColor(QPalette::WindowText, QRgb(0x404044));
        } else if (theme == QChart::ChartThemeHighContrast) {
            pal.setColor(QPalette::Window, QRgb(0xffab03));
            pal.setColor(QPalette::WindowText, QRgb(0x181818));
        } else if (theme == QChart::ChartThemeBlueIcy) {
            pal.setColor(QPalette::Window, QRgb(0xcee7f0));
            pal.setColor(QPalette::WindowText, QRgb(0x404044));
        } else {
            pal.setColor(QPalette::Window, QRgb(0xf0f0f0));
            pal.setColor(QPalette::WindowText, QRgb(0x404044));
        }
        window()->setPalette(pal);
    }else{
        for (QChartView *chartView : charts) {
            chartView->chart()->setTheme(theme);
        }
    }

    // Update antialiasing
    bool checked = m_ui->antialiasCheckBox->isChecked();
    for (QChartView *chart : charts)
        chart->setRenderHint(QPainter::Antialiasing, checked);

    // Update animation options
    QChart::AnimationOptions options(
                m_ui->animatedComboBox->itemData(m_ui->animatedComboBox->currentIndex()).toInt());
    if (!m_charts.isEmpty() && m_charts.at(0)->chart()->animationOptions() != options) {
        for (QChartView *chartView : charts)
            chartView->chart()->setAnimationOptions(options);
    }

    // Update legend alignment
    Qt::Alignment alignment(
                m_ui->legendComboBox->itemData(m_ui->legendComboBox->currentIndex()).toInt());

    if (!alignment) {
        for (QChartView *chartView : charts)
            chartView->chart()->legend()->hide();
    } else {
        for (QChartView *chartView : charts) {
            chartView->chart()->legend()->setAlignment(alignment);
            chartView->chart()->legend()->show();
        }
    }


}

void ThemeWidget::redrawChart(){
    qDebug()<<"redraw chart";
    chartView = new QChartView(createLineChart());
    chartView->setRubberBand(QChartView::RectangleRubberBand);
    //    chartView->chart()->scroll(0,1);
    m_ui->gridLayout->addWidget(chartView, 1, 0);
    m_charts << chartView;

    chartView = new QChartView(createBarChart(m_valueCount));
    chartView->setRubberBand(QChartView::RectangleRubberBand);
    //    chartView->chart()->scroll(0,1);
    m_ui->gridLayout->addWidget(chartView, 2, 0);
    m_charts << chartView;

    chartView->chart()->zoomReset();


    updateUI();
}

