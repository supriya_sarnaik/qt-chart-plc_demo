// LslApi.h

#ifndef _LSLAPI_H_
#define _LSLAPI_H_
#include <windows.h>

typedef unsigned long _cdecl CB_FUNCTYPE(unsigned long);

extern "C" BOOL WINAPI Online(
    const char* szComm, 
    BYTE uBaudRate, 
    BYTE uPcStation, 
    BYTE uSpsStation, 
    BYTE uAutoInit
    );
extern "C" void WINAPI Offline(void);
extern "C" BOOL WINAPI IsOnline(void);
extern "C" BOOL WINAPI ModemOpen(void);
extern "C" DWORD WINAPI ModemGetNumber(void);
extern "C" BOOL WINAPI ModemGetName(
    DWORD dModemID, 
    char *pszName, 
    WORD maxsize
    );  
extern "C" BOOL WINAPI ModemCall(
    DWORD dModemID, 
    BYTE uType, 
    DWORD dConfigBaud, 
    char *szTelNr, 
    BYTE nTimeout
    );  
extern "C" void WINAPI ModemClose(void);
extern "C" BOOL WINAPI LslGetAdressVar(
    const char *name,
    long* adresse
    );
extern "C" BOOL WINAPI SetData(
    const void *pBuffer, 
    unsigned long addr0, 
    WORD nCount
    );
extern "C" BOOL WINAPI GetData(
    void *pBuffer, 
    unsigned long addr0, 
    WORD nCount
    );  
extern "C" BOOL WINAPI SendDataList(
    unsigned short length0, 
    char* data0, 
    CB_FUNCTYPE* callback
    );
extern "C" BOOL WINAPI GetDataList(char* data0);
extern "C" BOOL WINAPI GetCpuStatus(BYTE *pStatus);
extern "C" BOOL WINAPI LslGetPLCInfo(unsigned char* infostr, unsigned long maxsize0);
extern "C" BOOL WINAPI LslGetProjectInfo(void* pRetData0);
extern "C" BOOL WINAPI SetCommand(BYTE uCommand);

#endif

