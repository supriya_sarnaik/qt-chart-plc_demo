#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <processor.h>
#include <QtWidgets/QApplication>
#include <QFile>
#include <QTextStream>
#include <QDateTime>

void myMessageHandler(QtMsgType type, const QMessageLogContext &, const QString & msg)
{
    QString txt;
    switch (type) {
    case QtDebugMsg:
        txt = QString("Debug: %1").arg(msg);
        break;
    case QtWarningMsg:
        txt = QString("Warning: %1").arg(msg);
        break;
    case QtCriticalMsg:
        txt = QString("Critical: %1").arg(msg);
        break;
    case QtFatalMsg:
        txt = QString("Fatal: %1").arg(msg);
        abort();
    default:
      break;
    }
    QFile outFile("log.txt");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");
    ts << txt << Qt::endl;
}

int main(int argc, char *argv[])
{
    Processor processor;
//    QGuiApplication app(argc, argv); //removed in order to use Qtcharts
    QApplication app(argc, argv);
    qInstallMessageHandler(myMessageHandler);
    QQmlApplicationEngine engine;
 /*
  * Use below line Make class available in QML using engine.
  * */
    engine.rootContext()->setContextProperty("processor", &processor);
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    engine.load(url);

    return app.exec();
}
