import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Extras 1.4
import QtQuick.Dialogs 1.3
import QtGraphicalEffects 1.12
import QtQuick.Layouts 1.11
import QtCharts 2.3



Window {
    id: window
    width: 600
    height: 583
    opacity: 1
    visible: true
    color:"#2A2B42" //"#ebe1e1"
    property alias imageSource: image.source
    title: qsTr("PLC Connection")
    Rectangle {
        id: mainRectangle
        color: "#00000000"
        radius: 20
        border.width: 1
        anchors.fill: parent
        anchors.rightMargin: 3
        anchors.leftMargin: 7
        anchors.bottomMargin: 5
        anchors.topMargin: 0
        gradient: Gradient {
            GradientStop { position: 1.0; color:"#D3D3D3" } //"#808080"
            GradientStop { position: 0.33; color:"#D3D3D3"} //"#A9A9A9"
            GradientStop { position: 0.0; color: "#D3D3D3" }//"#D3D3D3"
        }

        Connections {
            target: processor

            function onConnectSignal(str) {
                console.log("Received signal from my class with string: " + str)
                button.text = str;
            }
            function onReadSignal(paramName,value){
                console.log("Received signal for read data signal: " + paramName + "Value: "+ value)
                if(paramName === "Traverse1.AnzAnlageBereit"){
                    if(value === 1){
                        readyBtnBg.color = "#50F210";
                    }
                }else if(paramName === "DMSimulator1.soMesswert"){
                    label6.text = value;
                }else if(paramName === "DehnelementSteuerung1.AnzDickeMittelwert"){
                    label9.text = value;
                    label12.text = value;
                }else if(paramName === "ProduktParameter1.isSetEqualAvg"){
                    //                    button5.text = value;
                }else if(paramName === "ProduktParameter1.siPlusToleranz"){
                    label21.text = value;
                }else if(paramName === "ProduktParameter1.Dickesollwert"){
                    label24.text = value;
                }else if(paramName === "ProduktParameter1.siMinusToleranz"){
                    label27.text = value;
                }else if(paramName === "ProduktParameter1.soNettobreite"){
                    label30.text = value;
                }else if(paramName === "MainDicke1.siMessungStart" ){
                    if(value === 1){
                        measureBtnBg.color = "#1AA820";
                    }
                }
            }
            function onSendSignal(data){
                console.log("Received signal for send data signal: " + data)
            }
        }



        TextField {
            id: textFieldId
            anchors.left: parent.left
            anchors.right: button.left
            anchors.top: parent.top
            anchors.rightMargin: 16
            anchors.leftMargin: 19
            anchors.topMargin: 75
            layer.enabled: false
            placeholderText: qsTr("Type your ip here")
        }
        Button {
            id: button
            y: 75
            width: 105
            height: 40
            text: qsTr("Connect")
            anchors.left: parent.left
            anchors.leftMargin: 161
            font.bold: true
            flat: false
            highlighted: false
            palette.buttonText: "#1e252c"
            property string ip: "127.0.0.1";
            signal connectSignalName(string ip);
            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                color: button.down ? "#d6d6d6" : "#f6f6f6"
                border.color: "#26282a"
                border.width: 0
                radius: 4
                layer.enabled: true
                layer.effect: DropShadow {
                    transparentBorder: true
                    horizontalOffset: 4
                    verticalOffset: 4
                    radius: 4.0
                }
            }

            onClicked:
            {
                onConnectSignalName: {
                    button.ip = textFieldId.text;
                    processor.connectSlot( button.ip)
                }
            }
        }
        Button {
            id: sendDataBtn
            text: qsTr("Send data to PLC")
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 59
            anchors.rightMargin: 15
            font.bold: true
            palette.buttonText: "#1e252c"
            property string data: "0";
            signal sendSignalName(string data);
            x: 397
            y: 468
            width: 178
            height: 40
            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                color: sendDataBtn.down ? "#d6d6d6" : "#f6f6f6"
                border.color: "#26282a"
                border.width: 0
                radius: 4
                layer.enabled: true
                layer.effect: DropShadow {
                    transparentBorder: true
                    horizontalOffset: 4
                    verticalOffset: 4
                    radius: 4.0
                }
            }
            onClicked:
            {
                onSendSignalName: {
                    sendDataBtn.data = valueTextField.text;
                    processor.sendSlot(comboBox.currentText,sendDataBtn.data)
                }
            }
        }


        MessageDialog
        {
            id: msgDialog
            title: "Warning"
            text: "Not Connected"
            informativeText: "The connection is not established"
            visible: false

            onAccepted: console.log("client clicked ok")
        }
        Connections{
            target: processor
            function onShowMessageBox(){
                msgDialog.visible = true
            }
        }

        Button {
            id: readBtnId
            height: 40
            text: qsTr("Read data from PLC")
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.topMargin: 75
            anchors.rightMargin: 15
            font.bold: true
            palette.buttonText: "#1e252c"
            property string paramName:"";
            signal readSignalName(string paramName);
            x: 397
            width: 178
            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                color: readBtnId.down ? "#d6d6d6" : "#f6f6f6"
                border.color: "#26282a"
                border.width: 0
                radius: 4
                layer.enabled: true
                layer.effect: DropShadow {
                    transparentBorder: true
                    horizontalOffset: 4
                    verticalOffset: 4
                    radius: 4.0
                }
            }
            onClicked:
            {
                onReadSignalName: {
                    console.log("read signal called");
                    processor.fileReadSlot()
                }
            }
        }

        Rectangle {
            id: rectangle
            color: "#00000000"
            radius: 2
            border.width: 1
            anchors.fill: parent
            anchors.rightMargin: 5
            anchors.leftMargin: 5
            anchors.bottomMargin: 28
            anchors.topMargin: 5

            Rectangle {
                id: rectangle1
                height: 57
//                color: "#00000000"
                color: "#067BD1"
                radius: 10
                border.width: 1
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin: 8
                anchors.rightMargin: 8
                anchors.leftMargin: 8
//                gradient: Gradient {
//                    GradientStop { position: 0.0; color: "#132755" }
//                    GradientStop { position: 0.33; color: "#2E6FB1" }
//                    GradientStop { position: 1.2; color: "#83D8F1" }
//                }

                Label {
                    id: label1
                    color: "#f9fbfc"
                    text: qsTr("NextGen HMI")
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.family: "Verdana"
                    font.pointSize: 16
                    font.styleName: "Bold"
                    font.weight: Font.ExtraBold

                    Label {
                        id: versionLabel
                        color: "#f9fbfc"
                        text: qsTr("v0.01")
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignVCenter
                        anchors.rightMargin: 0
                        anchors.leftMargin: 521
                        anchors.bottomMargin: 0
                        anchors.topMargin: 0
                        font.pointSize: 9
                    }
                }
            }

            Image {
                id: image
                y: 12
                width: 54
                height: 50
                anchors.left: parent.left
                source: "LM_Logo.png"
                anchors.leftMargin: 6
                fillMode: Image.PreserveAspectFit
            }

            Button {
                id: meaureBtnId
                y: 71
                text: qsTr("Measure")
                anchors.right: parent.right
                anchors.rightMargin: 212
                font.bold: true
                palette.buttonText: "#1e252c"
                property string servername : "";
                signal readSignalName();
                x: 268
                background: Rectangle {
                    id: measureBtnBg
                    implicitWidth: 100
                    implicitHeight: 40
                    color: meaureBtnId.down ? "#d6d6d6" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 0
                    radius: 4
                    layer.enabled: true
                    layer.effect: DropShadow {
                        transparentBorder: true
                        horizontalOffset: 4
                        verticalOffset: 4
                        radius: 4.0
                    }
                }
                onClicked:
                {
                    onreadSignalName: {
                        servername = "MainDicke1.siMessungStart";
                        processor.readSlot(servername)
                    }
                }
            }

            Button {
                id: readyButton
                y: 481
                width: 125
                height: 34
                text: qsTr("Ready")
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.leftMargin: 8
                anchors.bottomMargin: 30
                font.bold: true
                flat: false
                palette.buttonText: "#1e252c"
                highlighted: false
                background: Rectangle {
                    id: readyBtnBg
                    implicitWidth: 100
                    implicitHeight: 40
                    color: button.down ? "#d6d6d6" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 0
                    radius: 4
                    layer.enabled: true
                    layer.effect: DropShadow {
                        transparentBorder: true
                        horizontalOffset: 4
                        verticalOffset: 4
                        radius: 4.0
                    }
                }
            }

            Button {
                id: button1
                x: 395
                width: 177
                text: qsTr("")
                anchors.right: parent.right
                anchors.top: meaureBtnId.bottom
                anchors.bottom: button2.top
                anchors.topMargin: 8
                anchors.rightMargin: 8
                anchors.bottomMargin: 6
                background: Rectangle {
                    id: btn1Bg
                    implicitWidth: 100
                    implicitHeight: 40
                    color: button.down ? "#d6d6d6" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 0
                    radius: 4
                    layer.enabled: true
                    layer.effect: DropShadow {
                        transparentBorder: true
                        horizontalOffset: 4
                        verticalOffset: 4
                        radius: 4.0
                    }
                }

                Label {
                    id: label4
                    x: 8
                    y: 0
                    width: 144
                    height: 18
                    color: "#1e252c"
                    text: qsTr("actual measure")
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                    font.bold: false
                }

                Label {
                    id: label5
                    x: 150
                    y: 3
                    width: 26
                    height: 29
                    color: "#1e252c"
                    text: "[μm]"
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                }
                Label {
                    id: label6
                    x: 10
                    y: 12
                    width: 140
                    height: 20
                    color: "#1e252c"
                    text: qsTr("00.00")
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 11
                    font.bold: true
                }
            }

            Button {
                id: button2
                x: 395
                width: 177
                text: qsTr("")
                anchors.right: parent.right
                anchors.top: rectangle1.bottom
                anchors.bottom: button3.top
                anchors.topMargin: 92
                anchors.rightMargin: 8
                anchors.bottomMargin: 6
                background: Rectangle {
                    id: btn2Bg
                    implicitWidth: 100
                    implicitHeight: 40
                    color: button.down ? "#d6d6d6" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 0
                    radius: 4
                    layer.enabled: true
                    layer.effect: DropShadow {
                        transparentBorder: true
                        horizontalOffset: 4
                        verticalOffset: 4
                        radius: 4.0
                    }
                }
                Label {
                    id: label7
                    x: 8
                    y: 0
                    width: 144
                    height: 18
                    color: "#1e252c"
                    text: qsTr("average")
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                    font.bold: false
                }

                Label {
                    id: label8
                    x: 150
                    y: 3
                    width: 26
                    height: 29
                    color: "#1e252c"
                    text: "[μm]"
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                }
                Label {
                    id: label9
                    x: 10
                    y: 12
                    width: 140
                    height: 20
                    color: "#1e252c"
                    text: qsTr("00.00")
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 11
                    font.bold: true
                }

            }

            Button {
                id: button3
                x: 395
                y: 195
                width: 177
                height: 32
                text: qsTr("")
                anchors.right: parent.right
                anchors.bottom: button5.top
                anchors.bottomMargin: 44
                anchors.rightMargin: 8
                background: Rectangle {
                    id: btn3Bg
                    implicitWidth: 100
                    implicitHeight: 40
                    color: button.down ? "#d6d6d6" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 0
                    radius: 4
                    layer.enabled: true
                    layer.effect: DropShadow {
                        transparentBorder: true
                        horizontalOffset: 4
                        verticalOffset: 4
                        radius: 4.0
                    }
                }
                Label {
                    id: label10
                    x: 8
                    y: 0
                    width: 144
                    height: 18
                    color: "#1e252c"
                    text: qsTr("average")
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                    font.bold: false
                }

                Label {
                    id: label11
                    x: 150
                    y: 3
                    width: 26
                    height: 29
                    color: "#1e252c"
                    text: "[μm]"
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                }
                Label {
                    id: label12
                    x: 10
                    y: 12
                    width: 140
                    height: 20
                    color: "#1e252c"
                    text: qsTr("00.00")
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 11
                    font.bold: true
                }
            }

            Button {
                id: button5
                x: 395
                y: 271
                width: 177
                height: 32
                text: qsTr("")
                anchors.right: parent.right
                anchors.bottom: button6.top
                anchors.rightMargin: 8
                anchors.bottomMargin: 6
                background: Rectangle {
                    id: btn5Bg
                    implicitWidth: 100
                    implicitHeight: 40
                    color: button.down ? "#d6d6d6" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 0
                    radius: 4
                    layer.enabled: true
                    layer.effect: DropShadow {
                        transparentBorder: true
                        horizontalOffset: 4
                        verticalOffset: 4
                        radius: 4.0
                    }
                }
                Label {
                    id: label16
                    x: 8
                    y: 0
                    width: 161
                    height: 32
                    color: "#1e252c"
                    text: qsTr("set value = average value")
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.bold: false
                }

            }

            Button {
                id: button6
                x: 395
                y: 309
                width: 177
                height: 32
                text: qsTr("")
                anchors.right: parent.right
                anchors.bottom: button7.top
                anchors.rightMargin: 8
                anchors.bottomMargin: 6
                background: Rectangle {
                    id: btn6Bg
                    implicitWidth: 100
                    implicitHeight: 40
                    color: button.down ? "#d6d6d6" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 0
                    radius: 4
                    layer.enabled: true
                    layer.effect: DropShadow {
                        transparentBorder: true
                        horizontalOffset: 4
                        verticalOffset: 4
                        radius: 4.0
                    }
                }
                Label {
                    id: label19
                    x: 8
                    y: 0
                    width: 144
                    height: 18
                    color: "#1e252c"
                    text: qsTr("+tolerance")
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                    font.bold: false
                }

                Label {
                    id: label20
                    x: 150
                    y: 3
                    width: 26
                    height: 29
                    color: "#1e252c"
                    text: "[μm]"
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                }
                Label {
                    id: label21
                    x: 10
                    y: 12
                    width: 140
                    height: 20
                    color: "#1e252c"
                    text: qsTr("00.00  ")
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignTop
                    font.pointSize: 11
                    font.bold: true
                }
            }

            Button {
                id: button7
                x: 395
                y: 347
                width: 177
                height: 32
                text: qsTr("")
                anchors.right: parent.right
                anchors.bottom: button8.top
                anchors.bottomMargin: 6
                anchors.rightMargin: 8
                background: Rectangle {
                    id: btn7Bg
                    implicitWidth: 100
                    implicitHeight: 40
                    color: button.down ? "#d6d6d6" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 0
                    radius: 4
                    layer.enabled: true
                    layer.effect: DropShadow {
                        transparentBorder: true
                        horizontalOffset: 4
                        verticalOffset: 4
                        radius: 4.0
                    }
                }
                Label {
                    id: label22
                    x: 8
                    y: 0
                    width: 144
                    height: 18
                    color: "#1e252c"
                    text: qsTr("set value")
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                    font.bold: false
                }

                Label {
                    id: label23
                    x: 150
                    y: 3
                    width: 26
                    height: 29
                    color: "#1e252c"
                    text: "[μm]"
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                }
                Label {
                    id: label24
                    x: 10
                    y: 12
                    width: 140
                    height: 20
                    color: "#1e252c"
                    text: qsTr("00.00  ")
                    horizontalAlignment: Text.AlignRight
                    font.pointSize: 11
                    font.bold: true
                }
            }

            Button {
                id: button8
                x: 395
                y: 385
                width: 177
                height: 32
                text: qsTr("")
                anchors.right: parent.right
                anchors.rightMargin: 8
                background: Rectangle {
                    id: btn8Bg
                    implicitWidth: 100
                    implicitHeight: 40
                    color: button.down ? "#d6d6d6" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 0
                    radius: 4
                    layer.enabled: true
                    layer.effect: DropShadow {
                        transparentBorder: true
                        horizontalOffset: 4
                        verticalOffset: 4
                        radius: 4.0
                    }
                }
                Label {
                    id: label25
                    x: 8
                    y: 0
                    width: 144
                    height: 18
                    color: "#1e252c"
                    text: qsTr("-tolerance")
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                    font.bold: false
                }

                Label {
                    id: label26
                    x: 150
                    y: 3
                    width: 26
                    height: 29
                    color: "#1e252c"
                    text: "[μm]"
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                }
                Label {
                    id: label27
                    x: 10
                    y: 12
                    width: 140
                    height: 20
                    color: "#1e252c"
                    text: qsTr("00.00  ")
                    horizontalAlignment: Text.AlignRight
                    font.pointSize: 11
                    font.bold: true
                }
            }

            Button {
                id: button9
                x: 395
                width: 177
                text: qsTr("")
                anchors.right: parent.right
                anchors.top: button8.bottom
                anchors.bottom: readyButton.top
                anchors.topMargin: 6
                anchors.bottomMargin: 26
                anchors.rightMargin: 8
                background: Rectangle {
                    id: btn9Bg
                    implicitWidth: 100
                    implicitHeight: 40
                    color: button.down ? "#d6d6d6" : "#f6f6f6"
                    border.color: "#26282a"
                    border.width: 0
                    radius: 4
                    layer.enabled: true
                    layer.effect: DropShadow {
                        transparentBorder: true
                        horizontalOffset: 4
                        verticalOffset: 4
                        radius: 4.0
                    }
                }
                Label {
                    id: label28
                    x: 8
                    y: 0
                    width: 144
                    height: 18
                    color: "#1e252c"
                    text: qsTr("net width")
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                    font.bold: false
                }

                Label {
                    id: label29
                    x: 150
                    y: 3
                    width: 26
                    height: 29
                    color: "#1e252c"
                    text: "[mm]"
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                }
                Label {
                    id: label30
                    x: 10
                    y: 12
                    width: 140
                    height: 20
                    color: "#1e252c"
                    text: qsTr("00.00  ")
                    horizontalAlignment: Text.AlignRight
                    font.pointSize: 11
                    font.bold: true
                }
            }

            TextField {
                id: valueTextField
                x: 285
                y: 481
                width: 105
                height: 34
                anchors.verticalCenter: comboBox.verticalCenter
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 30
                anchors.rightMargin: 190
                placeholderText: qsTr("Enter value")
            }

            ComboBox {
                id: comboBox
                y: 481
                height: 34
                anchors.left: parent.left
                anchors.right: valueTextField.left
                anchors.bottom: parent.bottom
                anchors.rightMargin: 6
                anchors.leftMargin: 139
                anchors.bottomMargin: 30
                model: ["MainDicke1.siMessungStart","Traverse1.AnzAnlageBereit", "DMSimulator1.soMesswert", "DehnelementSteuerung1.AnzDickeMittelwert",
                    "ProduktParameter1.isSetEqualAvg","ProduktParameter1.siPlusToleranz","ProduktParameter1.Dickesollwert","ProduktParameter1.siMinusToleranz",
                    "ProduktParameter1.soNettobreite"]
            }

            Rectangle {
                id: rectangle2
                color: "#ffffff"
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: meaureBtnId.bottom
                anchors.bottom: readyButton.top
                anchors.topMargin: 6
                anchors.bottomMargin: 15
                anchors.leftMargin: 15
                anchors.rightMargin: 195

                ChartView {
                    id: bar
                    y: 178
                    height: 172
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: -1
                    anchors.leftMargin: 0
                    anchors.rightMargin: -1
                    theme: ChartView.ChartThemeBrownSand
                    BarSeries {
                        BarSet {
                            label: "Set1"
                            values: [2, 2, 3]
                        }

                        BarSet {
                            label: "Set2"
                            values: [5, 1, 2]
                        }

                        BarSet {
                            label: "Set3"
                            values: [3, 5, 8]
                        }
                    }
                }
            }

            ChartView {
                id: line
                anchors.left: rectangle2.left
                anchors.right: rectangle2.right
                anchors.top: rectangle2.top
                anchors.bottom: rectangle2.bottom
                anchors.bottomMargin: 174
                anchors.topMargin: 0
                anchors.leftMargin: 0
                anchors.rightMargin: -1
                theme: ChartView.ChartThemeBrownSand
                LineSeries {
                    XYPoint {
                        x: 0
                        y: 2
                    }

                    XYPoint {
                        x: 1
                        y: 1.2
                    }

                    XYPoint {
                        x: 2
                        y: 3.3
                    }

                    XYPoint {
                        x: 5
                        y: 2.1
                    }
                }
            }

        }

        Label {
            id: label2
            color: "#1e252c"
            text: qsTr("© copyright 2022 Lohia Mechatronik")
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: rectangle.bottom
            anchors.bottom: parent.bottom
            anchors.rightMargin: 339
            anchors.leftMargin: 8
            anchors.bottomMargin: 9
            anchors.topMargin: 6
            font.pointSize: 9
            font.bold: false
        }

        Label {
            id: label3
            color: "#1e252c"
            text: qsTr("Contact us @ office@lohiamk.com")
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: rectangle.bottom
            anchors.bottom: parent.bottom
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            anchors.rightMargin: 5
            anchors.leftMargin: 311
            anchors.bottomMargin: 8
            anchors.topMargin: 7
            font.pointSize: 9
            font.bold: false
        }

    }
}


/*##^##
Designer {
    D{i:0;formeditorZoom:0.9}D{i:6}D{i:7}D{i:8}D{i:11}D{i:14}D{i:15}D{i:16}D{i:22}D{i:21}
D{i:20}D{i:23}D{i:24}D{i:27}D{i:33}D{i:34}D{i:35}D{i:30}D{i:39}D{i:40}D{i:41}D{i:36}
D{i:45}D{i:46}D{i:47}D{i:42}D{i:51}D{i:48}D{i:55}D{i:56}D{i:57}D{i:52}D{i:61}D{i:62}
D{i:63}D{i:58}D{i:67}D{i:68}D{i:69}D{i:64}D{i:73}D{i:74}D{i:75}D{i:70}D{i:76}D{i:77}
D{i:80}D{i:79}D{i:78}D{i:85}D{i:84}D{i:19}D{i:90}D{i:91}D{i:1}
}
##^##*/
