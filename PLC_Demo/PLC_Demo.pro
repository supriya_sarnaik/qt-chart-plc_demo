QT += quick
QT += widgets
QT += charts

CONFIG += c++11
CONFIG -= app_bundle
CONFIG+= static

# we make our code fail to compile if it uses deprecated APIs by using the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        processor.cpp

RESOURCES += qml.qrc

#LIBS = "C:/Users/Lenovo/Documents/QT_Workspace/PLC_Demo/Lasal32.dll"
#LIBS = "C:/Users/Lenovo/Documents/QT_Workspace/PLC_Demo/Lasal64.dll"

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    Lasal32.h \
    LslApi.h \
    lsl_stdhdr.h \
    lsl_stdint.h \
    processor.h

DISTFILES += \
    ../../../LM/LM_Logo.png \
    test
