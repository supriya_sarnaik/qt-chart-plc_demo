#ifndef PROCESSOR_H
#define PROCESSOR_H
#include <QObject>
#include <QDebug>
#include <QTimer>
class Processor : public QObject //Inheritance from QObject
{
    Q_OBJECT
public:
    explicit Processor(QObject *parent = 0);

    enum ChMode
    {
        CH_OBJ = 0,// object
        CH_CMD = 1, //command server
        CH_SVR = 2, //data server
        CH_CLT_CMD = 3,// command client
        CH_CLT_DATA = 4,// data client
        CH_CLT_OBJ = 5,//' object client
        CH_OBJ_DIR = 6,
        CH_CNST = 7,
        CH_NONE = 8,
        CH_CLT_EMB = 9,
    };
    Q_ENUM(ChMode)

signals:
    void connectSignal(QString str);
    void sendSignal(QString data);
    void readSignal(QString paramName, QString value);
    void showMessageBox();
    void fileReadSignal();

public slots:
    void connectSlot(QString str);
    void sendSlot(QString paramName,QString data);
    void readSlot(QString paramName);
    void fileReadSlot();
private:
    QTimer *timer;



};

#endif // PROCESSOR_H
