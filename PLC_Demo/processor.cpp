#include <QObject>
#include <QDebug>
#include "processor.h"
#include <Lasal32.h>
#include <string.h>
#include <QMessageBox>
#include <QLibrary>
#include <QFile>
#include <QDir>

/*
 *Using QLibrary make available the dll file in the application
 */
QLibrary library("C:/Users/Lenovo/Documents/QT_Workspace/qt-chart-plc_demo/PLC_Demo/Lasal32.dll");  //path used for local purpose
//QLibrary library(QDir::currentPath() + "/Lasal32.dll");
ChMode ModBuf = ChMode::_CH_SVR;
uint32_t MinimalBorderAdress = (uint32_t)0;
uint32_t MinimalBorderValue = (uint32_t)0;
char MyClassName[300];
bool ok;



/* Constructor.
 * Following tasks are acheived here -
 * 1. to load dll dynamically
 * 2. check if dll file is loaded or not
 * 3. if yes then print ok else print the error statement using errorstring.
 */
Processor::Processor(QObject *parent) :
    QObject(parent)
{

    library.load();

}

/*
 * Function : connectSlot()
 * Connection is acheived using this method.Below steps are followed -
 * 1. IsOnline function of lasal32 is used to check if system is online.
 * 2. Online function is used with ip as a request param to connect.
 * 3. if received true then connection successful else not connected.
 * 4. If connection successful, the time is started with 10 sec frequency
 * to start reading functionality.
 */
void Processor::connectSlot(QString str) {

    ok = library.isLoaded();
    if (ok) {
        qDebug() << "DLL loaded successfully" << Qt::endl;
    } else {
        qDebug() << "Unable to load DLL" << Qt::endl;
        qDebug() << QString::fromStdString(library.errorString().toStdString());
        qDebug() << QString::fromStdString(library.fileName().toStdString());
    }
    typedef void (*IsOnline)();
    IsOnline isOnlineFunction = (IsOnline) library.resolve("IsOnline");
    if (isOnlineFunction){
        if (!IsOnline())
        {
            std::string ipaddr = str.toStdString();  //ip address received from qml
            const char* IPAddress = ipaddr.c_str();// "TCP:192.168.0.52"
            std::string IP = std::string("TCP:").append(IPAddress);

            byte BaudByte = 0;
            byte SPSByte = 0;
            qDebug()<< "IP: " << IP.c_str();
            QString addr = IP.c_str();
            typedef bool (*Online)(const char*,uint8_t,uint8_t,uint8_t,uint8_t);
            Online onlineFunction = (Online) library.resolve("Online");
            if(onlineFunction){
                bool status = onlineFunction(IP.c_str(), BaudByte, SPSByte, 0, 0);
                qDebug() <<  "Online function connect slot - "<<status<<"\n";
                if (status)
                {
                    str =  "Disconnect";
                    qDebug() <<"Connection successful!!"<<"\n";
                    //                    emit connectSignal(str);
                    //                    timer = new QTimer(this);
                    //                    connect(timer, SIGNAL(timeout()), this, SLOT(fileReadSlot()));
                    //                    timer->start(100);

                }
                else
                {
                    str =  "Connect";
                    qDebug() <<"Connection unsuccessful.."<<"\n";
                    //                    emit connectSignal(str);
                }

            }else{
                qDebug() <<"In else online function"<<"\n";
            }

        }else{
            qDebug() <<"In else isOnline function"<<"\n";
            //            emit showMessageBox();
        }

    }

}

/*
 * Function : sendSlot()
 * This function is used to send data to the device wrt the servername mapped. steps followed are -
 * 1. server name and data is accepted as request.
 * 2. server name is cnverted to const char* (ascii).
 * 3. Using isOnline function connection is verified.
 * 4. Using LslGetObject function the server param with address is sent.
 * 5. if true, using LslWriteToSvr data is written to specific address
 * 6. On success, the value is read from the same address and emitted.
 */
void Processor::sendSlot(QString paramName, QString data) {
  qDebug() <<"paramName :" <<paramName<<" : data : "<<data<<"\n";
    QByteArray ba = paramName.toLocal8Bit();
    const char *serverParam = ba.data();

    typedef void (*IsOnline)();
    IsOnline isOnlineFunction = (IsOnline) library.resolve("IsOnline");
    if (isOnlineFunction){
        if (!IsOnline())
        {
            qDebug() <<"Isonline function send slot"<<"\n";
            uint32_t* tempPtr = &MinimalBorderAdress;
            ChMode* tempModBuf = (ChMode*) &ModBuf;

            typedef bool (*LslGetObject)(const char*,uint32_t*,ChMode*,char*);
            LslGetObject lslGetObjectFunction = (LslGetObject) library.resolve("LslGetObject");
            if(lslGetObjectFunction){
                qDebug() <<"In LslGetObject function send slot "<<"\n";
                lslGetObjectFunction(serverParam, tempPtr, tempModBuf, MyClassName); //"MyPumpControl1.MinimalValue" //ProduktParameter1.Dickesollwert

                    uint32_t* tempMinimalBorderValue = &MinimalBorderValue;
                    typedef bool (*LslWriteToSvr)(uint32_t,uint32_t);
                    LslWriteToSvr lslWriteToSvrFunction = (LslWriteToSvr) library.resolve("LslWriteToSvr");
                    if(lslWriteToSvrFunction){

                        lslWriteToSvrFunction(MinimalBorderAdress, data.toUInt());

                            typedef bool (*LslReadFromSvr)(uint32_t,uint32_t*);
                            LslReadFromSvr lslReadFromSvrFunction = (LslReadFromSvr) library.resolve("LslReadFromSvr");

                            if(lslReadFromSvrFunction){
                                qDebug() <<"Minimal border address : "<<MinimalBorderAdress ;
                                lslReadFromSvrFunction(MinimalBorderAdress, tempMinimalBorderValue);

                                    uint32_t val = *tempMinimalBorderValue;
                                    qDebug() << "val : "<<val;
                                    QString qstr = QString::fromStdString(std::to_string(val));
                                    qDebug() << "serverParam : "<< ba<<" value: "<< qstr<<"\n";
                                    //                                    emit sendSignal(QString::number(val));

                            }else{
                                qDebug() <<"In else lslReadFromSvr function"<<"\n";
                            }

                    }else{
                        qDebug() <<"In else lslWriteToSvr function"<<"\n";
                    }

            }else{
                qDebug() <<"In else lslGetObject function"<<"\n";
            }
        }else{
            qDebug() <<"In else isOnline function"<<"\n";
        }
    }

}

/*
 * Function : readSlot()
 * This function is used to read data from the device. below steps are followed -
 * 1. server name is accespted as a request.
 * 2. server name converted to const char *.
 * 3. connection verified using IsOnline function.
 * 4. object is obtained using LslGetObject function.
 * 5. On success, using LslReadFromSvr data is read from particular address.
 * 6. data is then emitted to UI to be displayed at specific location.
 *
 */
void Processor::readSlot(QString paramName) {
   qDebug() <<   "read slot called" <<"\n";
//    std::string serverName = paramName.toStdString();
//    const char* serverParam = serverName.c_str();
    QByteArray ba = paramName.toLocal8Bit();
    const char *serverParam = ba.data();
    qDebug()<<"param name : "<<serverParam;
    QString value = 0;
    typedef void (*IsOnline)();
    IsOnline isOnlineFunction = (IsOnline) library.resolve("IsOnline");
    if (isOnlineFunction){
        if (!IsOnline())
        {
            qDebug() <<"Isonline function read slot"<<"\n";
            uint32_t* tempPtr = &MinimalBorderAdress;
            ChMode* tempModBuf = (ChMode*) &ModBuf;
            //            char * tempMyclass = &MyClassName;

            typedef bool (*LslGetObject)(const char*,uint32_t*,ChMode*,char*);
            LslGetObject lslGetObjectFunction = (LslGetObject) library.resolve("LslGetObject");
            if(lslGetObjectFunction){
                qDebug() <<"In LslGetObject function read slot "<<"\n";
                 uint32_t status = lslGetObjectFunction(serverParam, tempPtr, tempModBuf, MyClassName); //ProduktParameter1.Dickesollwert //"MainDicke1.siMessungStart"
                qDebug() <<"status of lslGetObject function : "<<status<<"\n";
//                if(status == 1){
                    uint32_t* tempMinimalBorderValue = &MinimalBorderValue;

                    typedef bool (*LslReadFromSvr)(uint32_t,uint32_t*);
                    LslReadFromSvr lslReadFromSvrFunction = (LslReadFromSvr) library.resolve("LslReadFromSvr");

                    if(lslReadFromSvrFunction){
                        qDebug()<<"Minimal border address : "<< MinimalBorderAdress;
                        uint32_t status = lslReadFromSvrFunction(MinimalBorderAdress, tempMinimalBorderValue);
//                        qDebug() <<"status of lslReadFromSvr "<<status<<"\n";
//                        if(status){
                            uint32_t val = *tempMinimalBorderValue;
                            qDebug() << " returned val : "<<val<<"\n";

//                            value = QString::number(val);
                            QString qstr = QString::fromStdString(std::to_string(val));
                             qDebug() << "serverParam : "<< ba <<" value: "<< qstr<<"\n";
//                                                        emit readSignal(paramName,value);
//                        }else{
//                            qDebug() << "in else status of lslReadFromSvr"<<"\n";

//                            //                            emit readSignal(paramName,QString::number(MinimalBorderValue));
//                        }
                    }else{
                        qDebug() <<"In else lslReadFromSvr function"<<"\n";
                    }
//                }else{
//                     qDebug() << " value : "<<"a"<<"\n";
////                    qDebug() <<"In else status of lslGetObject function serverParam : "<< serverParam<<" val : "<<MinimalBorderValue<<"\n";
//                    //                    emit readSignal(paramName,QString::number(MinimalBorderValue));
//                }

            }else{
                qDebug()<<"In else lslGetObject function"<<"\n";
            }

        }else{
            qDebug()<<"In else isOnline function"<<"\n";
        }
    }else{
        qDebug()<<"IsOnlineFunction false";
    }

}

/*
 *  Function : fileReadSlot()
 *  This function is used to read the file containing the server names.
 *  1. File is created with all the server names.
 *  2. Read line by line.
 *  3. Per line read function is called to fetch the data.
 *  4.operation continues till the EOF.
 */
void Processor :: fileReadSlot(){
    qDebug()<<"file slot called";
//        QFile file(":/serverNameFile.txt");
    QFile file(QDir::currentPath() +"/serverNameFile.txt");
    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "error opening file: " << file.error();
        return;
    }
    QTextStream stream(&file);
    while (!stream.atEnd()){
        QString str = stream.readLine();
        readSlot(str);
    }
    file.close();
}
